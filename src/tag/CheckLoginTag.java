package tag;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class CheckLoginTag extends SimpleTagSupport {
	
	private static final String LOGIN = "login";
	private static final String LOGOUT = "logout";
	private static final String TEMPLATE = "<a href='/%s/users/%s'>%s</a>";
	public void doTag() throws JspException, IOException {

		HttpServletRequest request = (HttpServletRequest) ((PageContext) getJspContext()).getRequest();
		String ApplicationName = request.getContextPath().replace("/", "");
		JspWriter out = getJspContext().getOut();
		if(request.getRemoteUser() != null) {
			out.print(String.format(TEMPLATE, ApplicationName, LOGOUT, LOGOUT));
		} else {
			out.print(String.format(TEMPLATE, ApplicationName, LOGIN, LOGIN));
		}
	}
}
