package utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.tomcat.jdbc.pool.DataSource;

public class DBService {
	private static final String RESURS_CLOSING_ERR = "Resource closing problem : ";
	private static DataSource ds;
	public static Connection getConnection() throws NamingException, SQLException {
		if(ds == null) {
			InitialContext initContext = new InitialContext();
			ds = (DataSource) initContext.lookup("java:comp/env/jdbc/users");
		}
		return ds.getConnection(); 
	}
	
	public static void closeStatements(Statement... resurses) {
		for (int i = 0; i < resurses.length; i++) {
			if (resurses[i] != null) {
				try {
					resurses[i].close();
				} catch (SQLException e) {
					System.err.println(RESURS_CLOSING_ERR + e);
				}
			}
		}
	}

	public static void closeResultSet(ResultSet... resurses) {
		for (int i = 0; i < resurses.length; i++) {
			if (resurses[i] != null) {
				try {
					resurses[i].close();
				} catch (SQLException e) {
					System.err.println(RESURS_CLOSING_ERR + e);
				}
			}
		}
	}
	public static void closeConnection(Connection conn) {
		
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					System.err.println(RESURS_CLOSING_ERR + e);
				}
			
		}
	}
	

}
