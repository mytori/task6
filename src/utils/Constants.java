package utils;

public class Constants {
	public static final String LOGIN = "login";
	public static final String PASSWORD = "password";
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String INFO = "info";
	public static final String USER = "user";
	
}
