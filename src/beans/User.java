package beans;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.servlet.http.HttpSessionEvent;



public class User implements Serializable {
	
	private static final long serialVersionUID = -6904991735545441746L;

	private static final Logger log = Logger.getLogger(User.class.getName());
	private static final String DEFAULT_ROLE = "user";
	
	private String firstName;
	private String lastName;
	private String login;
	private String password;
	private String role;
	private String info;
	
	
	public User(String firstName, String lastName, String login, String password, String info) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.login = login;
		this.password = password;
		this.info = info;
		this.role = DEFAULT_ROLE;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	
	
	private static void logSession(String msg, HttpSessionEvent se) {
    	log.info(msg);
        log.info(se.getSession().getId());
        User user = (User) se.getSession().getAttribute("User");
        if(user != null) {
        	  log.info("user - " + user.getLogin());
        }
    }

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	
}
