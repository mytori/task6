package servlets;

import java.io.BufferedReader;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import dao.UsersDaoImpl;


public class UsersDeleteControler extends HttpServlet {
	
	
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UsersDaoImpl dao = new UsersDaoImpl();
		try {
			BufferedReader reader = request.getReader();
			JSONObject entityJson = (JSONObject) JSONValue.parseWithException(reader);
			String login = (String) entityJson.get("login");
			dao.delete(login);
		} catch (Exception e) {
			throw new ServletException(e.getMessage());
		}

	}
}
