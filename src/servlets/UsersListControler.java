package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Logger;

import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import dao.UsersDaoImpl;
import utils.Constants;


public class UsersListControler extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = Logger.getLogger(UsersListControler.class.getName());

	


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		log.info("In UserListControler");
		List<User> users = null;
		try {
			
			UsersDaoImpl dao = new UsersDaoImpl();
			users = dao.getUsers();
			RequestDispatcher dispatcher = request.getRequestDispatcher("/management/userList.jsp");
			request.setAttribute("users", users);
		
			dispatcher.forward(request, response);	
			
		} catch (SQLException | NamingException e) {
			log.info(e.getMessage());
			throw new ServletException(e);
		}


	}




}
