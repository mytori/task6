var Delete = (function() {

	var buttonDelete = document.getElementsByName("delete");
	var $this;

	return {
		init : function() {
			$this = Delete;	
			for(var i = 0; i <buttonDelete.length; i++) {
				buttonDelete[i].addEventListener('click', function(e) {e.preventDefault(); Delete.send(e);});
			}
		},
		send : function(e) {
			var req = Request.getRequest();
			var param = getJsonForDelete(e);
			var res;
			if (req != undefined) {
				req.open("DELETE", 'user-delete', true); 
				req.setRequestHeader('Content-type', 'application/json; charset=utf-8');
				req.onreadystatechange = function() {
					if (req.readyState == 4) {
						if (req.status == 200) {
							res = req.responseText;
						}
					} else {
						alert(req.status + ': ' + req.statusText);
					}
						};
						req.send(param);
					
					
				}
			
		}

	};


	function addField(entity, field) {
		if(field.multiple){
			var options = field.options;
			var result = [];
			
			for(var j = 0, k = 0; j<options.length; j++){
				if(options[j].selected){
					result[k] = options[j].value;
					k++;
				}
			}
			entity[field.name] = result.join(';');
			
		} else {
			entity[field.name] = field.value;
		}
		
	}
	

	function getJsonForDelete(e) {
		var entity = {
			login : e.target.getAttribute('data-login')
		};
		
		alert(JSON.stringify(entity));
		return JSON.stringify(entity);
	}

	

})();
document.addEventListener("DOMContentLoaded",function(){Delete.init();}); 