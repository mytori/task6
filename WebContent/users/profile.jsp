<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${sessionScope.user.firstName} ${sessionScope.user.lastName}'s profile</title>
</head>
<body>

	<h1>${sessionScope.user.firstName} ${sessionScope.user.lastName}'s profile</h1>
	<ul>
		<li><a href="/task1/async">Some special page</a></li>
		<li><a href="/task1/protected/upload.jsp">Upload page</a></li>
		<li><a href="/task/tag-test.jsp">Tag testing page</a></li>
	</ul>
	
	<form method="post" action="/task1/changeProfile">
 	<table> 
 		<tr>
 			<td><label for="firstname" >First name</label></td> 
 			<td><input id="firstname" name="firstName" value= ${sessionScope.user.firstName} ></td>
 		</tr>
 		<tr>
 			<td><label for="lastname" >Last name</label></td> 
 			<td><input id="lastname" name="lastName" value= ${sessionScope.user.lastName} ></td>
 		</tr>
 		<tr>
 			<td><label for="login" >Login</label></td> 
 			<td><input id="login" name="login" value= ${sessionScope.user.login} disabled></td>
 		</tr>
 		<tr>
 			<td><label for="pass" >Password</label></td> 
 			<td><input id="pass" name="password" value= ${sessionScope.user.password}></td>
 		</tr>
 		<tr>
 			<td><label for="info" >User information</label></td> 
 			<td><input id="info" name="info" value= ${sessionScope.user.info}></td>
 		</tr>
 	</table>
 	<button type="submit">Change</button> 
 	</form> 
</body>
</html>