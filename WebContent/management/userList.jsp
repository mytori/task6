<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type='text/javascript' src='/Java_web_security/scripts/initRequest.js'></script>
<script type='text/javascript' src='/Java_web_security/scripts/delete_user.js'></script>
<title>Users list</title>
</head>
<body>
	
	<table>
		<tr>
			<th>First name</th>
			<th>Last name</th>
			<th>Login</th>
			<th>Password</th>
			<th>Info</th>
		</tr>
		<c:forEach var="user" items="${requestScope.users}">
				<tr>
					<td>${user.firstName}</td>
					<td>${user.lastName}</td>
					<td>${user.login}</td>
					<td>${user.password}</td>
					<td>${user.info}</td>
					<td><button name="delete" data-login="${user.login}">Delete</button></td>
				</tr>
			</c:forEach>
	</table>
</body>
</html>